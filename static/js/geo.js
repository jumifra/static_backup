var map = L.map('map').fitWorld();

// L.tileLayer('https://yukon.prix.tips/tile/{z}/{x}/{y}.png', {
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    minZoom: 10,
    maxZoom: 18,
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

var markersLayer = L.layerGroup();
var positionLayer = L.layerGroup();

map.addLayer(markersLayer);
map.addLayer(positionLayer);

function dragEnd(ev) {
    var changedPos = ev.target.getLatLng();
    this.bindPopup("Area de Búsqueda").openPopup();
    var term = $("#auto_term").val();
    Searcher(term, JSON.stringify([changedPos.lat, changedPos.lng]));
}

function onLocationFound(e) {
    var radius = e.accuracy / 2;
    curMark = L.marker(e.latlng, {draggable: false}).addTo(map).bindPopup("Tu ubicación").openPopup();
    curMark.addTo(positionLayer);
    $('#coords').val([e.latlng.lat, e.latlng.lng]);;
    curMark.on('dragend', dragEnd);
    getMapData([e.latlng.lat, e.latlng.lng]);
}

function onLocationError(e) {
    $('#coords').val([]);
}

map.on('locationfound', onLocationFound);
map.on('locationerror', onLocationError);

map.on('dblclick', function() {
  if (map.scrollWheelZoom.enabled()) {
    map.scrollWheelZoom.disable();
    }
    else {
    map.scrollWheelZoom.enable();
    }
  });

$("#btn-products").on("click", function (e) {
    $(".list-result").show();
    $(".main-maps").hide();
});

$("#btn-locations").on("click", function (e) {
    $(".list-result").hide();
    $(".main-maps").show();
    map.invalidateSize(true);
    map.locate({setView: true, watch: false, enableHighAccuracy: false, maximumAge: 3000, maxZoom: 15});
});
