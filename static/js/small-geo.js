var $ = jQuery;

var map = L.map("mapid");

function onLocationFound(e) {
    var radius = e.accuracy / 2;
    $("#coords").val([e.latlng.lat, e.latlng.lng]);
}

function onLocationError(e) {
    $("#coords").val([]);
}

map.on("locationfound", onLocationFound);
map.on("locationerror", onLocationError);

map.locate({setView: true, watch: false, enableHighAccuracy: false, maxZoom: 18});
