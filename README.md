#Statics Service


##Repo

Get original changes

    git pull origin master

Push custom changes

    git push main master

Push with TAGs (after commit)

    git tag 5.0
    git push main 5.0

Push with TAGs (with commit as reference)

    git commit -m "Changes in README"
    git tag -f latest
    git push main -f latest

