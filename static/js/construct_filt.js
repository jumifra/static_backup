function load_store(list_store) {
    console.log('ffff')
    var ul_store = document.getElementsByClassName('chk_stores')[0];
    ul_store.innerHTML = '';
    for (let store of list_store) {
        ul_store.innerHTML += `
        <li class="resultFilters__int__drop__list">
            <label for="${store}" class="resultFilters__int__drop__list__label">
                <input type="checkbox" id="${store}" class="resultFilters__int__drop__list__label--button element_chk_store" value=${store}/>
                <p class="resultFilters__int__drop__list__label--text"><span class="resultFilters__int__drop__list__label--textSpan">${store}</span></p>
            </label>
        </li>
        `
    }
}

function load_brands(list_brands) {
    console.log(list_brands);
    var ul_brands = document.getElementsByClassName('chk_brands')[0];
    ul_brands.innerHTML = '';
    for (let brand of list_brands) {
        ul_brands.innerHTML += `
        <li class="resultFilters__int__drop__list">
            <label for="${brand}" class="resultFilters__int__drop__list__label">
                <input type="checkbox" id="${brand}" class="resultFilters__int__drop__list__label--button element_chk_store" value=${brand}/>
                <p class="resultFilters__int__drop__list__label--text"><span class="resultFilters__int__drop__list__label--textSpan">${brand}</span></p>
            </label>
        </li>
        
        `
    }
}
//
// <li class="resultFilters__int__drop__list" onclick="hiddestore()">
//     <label for="${brand}" class="resultFilters__int__drop__list__label" onclick="hiddestore()">
//         <input type="checkbox" id="${brand}" class="resultFilters__int__drop__list__label--button element_chk_store" onclick="hiddestore()" value=${brand}/>
//         <p class="resultFilters__int__drop__list__label--text"><span class="resultFilters__int__drop__list__label--textSpan" onclick="hiddestore()">${brand}</span></p>
//     </label>
// </li>

function load_range_prices(min_p, max_p) {
    var min = document.getElementById('min_price');
    var max = document.getElementById('max_price');
    var step = min.getAttribute('step');
    max_p += parseFloat(step)
    min.setAttribute("min", parseFloat(min_p));
    min.setAttribute("value", parseFloat(min_p));
    min.setAttribute("max", parseFloat(max_p));
    max.setAttribute("max", parseFloat(max_p));
    max.setAttribute("min", parseFloat(min_p));
    max.setAttribute("value", parseFloat(max_p));
    $('#span_min').html('');
    $('#span_min').html('S/ ' + min_p);
    $('#span_max').html('');
    $('#span_max').html('S/ ' + max_p);


}

function build_page(number_page, current_page){
    var pag = $(".pageSearch__content__main")[0]
    pag.innerHTML = '';
    if(current_page>3 && number_page>=6 ){
        if ((number_page - current_page) > 1){
            for (let i = current_page - 2; i <= current_page + 1; i++) {
                if (i == current_page){
                    pag.innerHTML += "<li class=\"pageSearch__content__main__list pageSearch__content__main__list--current\" value=\"" + i + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + i + "</a></li>"
                }
                else{
                    pag.innerHTML += "<li class=\"pageSearch__content__main__list\" value=\"" + i + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + i + "</a></li>"
                }
            }
            if((number_page - current_page) > 1){
                pag.innerHTML += " ... <li class=\"pageSearch__content__main__list\" value=\"" + number_page + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + number_page + "</a></li>"
            }

        }else{
            for (let i = current_page - 3; i <= current_page; i++) {
                if (i == current_page){
                    pag.innerHTML += "<li class=\"pageSearch__content__main__list pageSearch__content__main__list--current\" value=\"" + i + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + i + "</a></li>"
                }
                else{
                    pag.innerHTML += "<li class=\"pageSearch__content__main__list\" value=\"" + i + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + i + "</a></li>"
                }
            }
        }

    }
    else if (number_page>=6){
        for (let i = 1; i <= 4; i++) {
                if (i == current_page){
                    pag.innerHTML += "<li class=\"pageSearch__content__main__list pageSearch__content__main__list--current\" value=\"" + i + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + i + "</a></li>"
                }
                else{
                    pag.innerHTML += "<li class=\"pageSearch__content__main__list\" value=\"" + i + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + i + "</a></li>"
                }

        }
        pag.innerHTML += " ... <li class=\"pageSearch__content__main__list\" value=\"" + number_page + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + number_page + "</a></li>"
//            if(() > 1){
//                pag.innerHTML += " ... <li class=\"pageSearch__content__main__list\" value=\"" + number_page + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + number_page + "</a></li>"
//            }
    }
    else if(number_page < 6){
        for (let i = 1; i <= number_page; i++) {
                if (i == current_page){
                    pag.innerHTML += "<li class=\"pageSearch__content__main__list pageSearch__content__main__list--current\" value=\"" + i + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + i + "</a></li>"
                }
                else{
                    pag.innerHTML += "<li class=\"pageSearch__content__main__list\" value=\"" + i + "\" onclick=\"send_filter(this.value)\"><a class=\"pageSearch__content__main__list--link\">" + i + "</a></li>"
                }

        }
    }

}

function send_filter(page=null) {
    // INSTANCE VARIABLES
    var send_stores = [];
    var send_brands = [];
    var send_min_prices = 0.0;
    var send_max_prices = 0.0;

    // GET VALUES FOR VARIEBLES
    var span_min = document.getElementById('span_min').textContent;
    var span_max = document.getElementById('span_max').textContent;
    span_min = span_min.replace('S/ ', '');
    span_max = span_max.replace('S/ ', '');
    send_min_prices = parseFloat(span_min);
    send_max_prices = parseFloat(span_max);
    for (let chk_store of document.getElementsByClassName('element_chk_store')) {
        if (chk_store.checked) {
            send_stores.push(chk_store.value);
        }
    }

    for (let chk_brand of document.getElementsByClassName('element_chk_brands')) {
        if (chk_brand.checked) {
            send_brands.push(chk_brand.value);
        }
    }
    // BUILD URL
    var url = "https://prix.tips/search/" + $('title').text().split(' - ')[0] + '?';
    url += "&min=" + send_min_prices;
    url += "&max=" + send_max_prices;
    url += "&sto=" + send_stores;
    url += "&bra=" + send_brands;
    url += "&pag=" + page
    console.log(url);
    window.location=url;
    // SEND VARIABLES
    var data = JSON.stringify({
        "min_price": send_min_prices,
        "max_price": send_max_prices,
        "stores": send_stores,
        "brands": send_brands
    });
}


////////////drop menu//////////////
//$(document).on("click", function(e){
//        console.log(e);
//        var name_class = e.target.dataset.name;
//        var name_id = e.target.dataset.id;
//        // console.log(name_class);
//        if(name_class == "menu"){
//            // console.log('iiiii');
//          for(let ules of $(".resultFilters__int__drop")){
//              console.log(ules.id);
//            if(ules.dataset.name != name_id){
//              id = '#' + ules.id;
//              $(id).css({'display':'None'});
//              $(id).val('hidde');
//            }
//            else{
//              id = '#' + ules.id;
//              if($(id).val() == 'hidde'){
//                $(id).css({'display':'block'});
//                $(id).val("show");
//              }
//              else{
//                $(id).css({'display':'None'});
//                $(id).val("hidde");
//              }
//            }
//          }
//        }
//        else if(name_class == "li"){
//          var name_id = e.target.dataset.fa;
//          for(let ules of $(".resultFilters__int__drop")){
//            if(ules.dataset.name != name_id){
//              id = '#' + ules.id;
//              console.log(id)
//              $(id).css({'display':'None'});
//              $(id).val('hidde');
//            }
//          }
//        }
//        else{
//          for(let ules of $(".resultFilters__int__drop")){
//            id = '#' + ules.id;
//            $(id).css({'display':'None'});
//            $(id).val('hidde');
//          }
//        }
//
//    });
////////////////////////////////////////

function hiddebrand(){
    console.log('hidde brand');
    $('#checkboxesdos').css({'display': 'None'})
}

function hiddestore(){
    console.log('hidde store');
    $('#checkboxes').css({'display': 'None'})
}

