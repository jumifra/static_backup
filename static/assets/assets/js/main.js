jQuery(document).ready(function(j) {

	"use strict";

	[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
		new SelectFx(el);
	} );

	jQuery('.selectpicker').selectpicker;


	j('#menuToggle').on('click', function(event) {
		j('body').toggleClass('open');
	});

	j('.search-trigger').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		j('.search-trigger').parent('.header-left').addClass('open');
	});

	j('.search-close').on('click', function(event) {
		event.preventDefault();
		event.stopPropagation();
		j('.search-trigger').parent('.header-left').removeClass('open');
	});

	// j('.user-area> a').on('click', function(event) {
	// 	event.preventDefault();
	// 	event.stopPropagation();
	// 	j('.user-menu').parent().removeClass('open');
	// 	j('.user-menu').parent().toggleClass('open');
	// });


});
