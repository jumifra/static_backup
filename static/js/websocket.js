function WsCom(validate) {
    var ws = new WebSocket("wss://" + document.domain + ":" + location.port + "/results/" + validate);
    ws.onopen = function (evt) {
        ws.send(validate);
    };
    ws.onmessage = function (evt) {
        data_val = evt.data;
        data = jQuery.parseJSON(data_val);
        mapData.push(data);
    };
    ws.onclose = function (e) {
        console.log("Socket is closed. Reconnect will be attempted in 1 second.", e.reason);
        setTimeout(function () {
            ws.onopen();
        }, 1000);
    };

    ws.onerror = function (err) {
        console.error("Socket encountered error: ", err.message, "Closing socket");
        ws.close();
    };
}
