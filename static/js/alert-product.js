function alert_product(event){
    alertType = $(event).attr("data-type");
    productName = $(event).attr("data-product");
    var coords = $("#coords").val();

    console.log(alertType + " - " + productName);

    $.ajax({
        url: "/alert-product",
        dataType: "json",
        method: 'post',
        data: {
            coords: coords,
            product_name: productName,
            alert_type: alertType,
        },
        success: function (response) {
            $.notify({
                        "message": response.message
                    },
                    {
                        "allow_dismiss": false,
                        "mouse_over": "pause",
                        "placement": {
                            "from": "bottom",
                            "align": "center"
                        },
                        "z_index": 1060,
                        "type": response.type,
                    }
            );
        }
        });
}