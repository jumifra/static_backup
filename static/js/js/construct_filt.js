function buildSlider(minPriceTotal, maxPriceTotal) {
    var disabled = false;
    if (minPriceTotal == null || maxPriceTotal == null) {
        minPriceTotal = 0;
        maxPriceTotal = 1000;
        disabled = true;
    }
    $('#span-slider').html(
        '<span id="min-price" data-currency="€" class="slider-price">S/ ' + minPriceTotal + '</span>\n' +
        '<div class="selector" id="selector">\n' +
        '    <div class="price-slider">\n' +
        '        <div id="slider-range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">\n' +
        '            <div class="ui-slider-range ui-corner-all ui-widget-header"></div>\n' +
        '            <span id="left-slide" tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span><span id="right-slide" tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>\n' +
        '        </div>\n' +
        '    </div> \n' +
        '</div>' +
        '<span id="max-price" data-currency="€" data-max="' + maxPriceTotal + '"  class="slider-price">S/ ' + maxPriceTotal + '</span>\n'
    );

    $("#slider-range").slider({
        range: true,
        min: parseInt(minPriceTotal),
        max: parseInt(maxPriceTotal),
        disabled: disabled,
        step: 1,
        slide: function (event, ui) {
            $("#min-price").html('S/ ' + ui.values[0]);

            suffix = '';
            if (ui.values[1] == $("#max-price").data('max')) {
                suffix = '';
            }
            $("#max-price").html('S/ '+ ui.values[1] + suffix);
        }
    });
    $('#left-slide').css("left", "0%");
    $('#right-slide').css("left", "500%");
    $('#right-slide').css("left", "100%");
    $(".ui-state-default").css("background-color", "#16CDCD");
    $(".ui-state-default").css("border-color", "#16CDCD");
    $("#span-slider").mouseup(function () {
        var event = new Event('change');
        document.getElementById('filter-min-price').dispatchEvent(event);
        var event = new Event('change');
        document.getElementById('filter-max-price').dispatchEvent(event);
        $('#filter-min-price').val(parseInt($('#min-price').text().replace('S/ ','')));
        $('#filter-max-price').val(parseInt($('#max-price').text().replace('S/ ','')));
    });
}

function addCompany(company) {
    var act = $('#filter-company').val();
    if (company.checked) {
        $('#filter-company').val(act + company.value + ',')
    } else {
        $('#filter-company').val(act.replace(company.value + ',', ''))
    }
    var event = new Event('change');
    document.getElementById('filter-company').dispatchEvent(event);
}

function addBrand(brand) {
    var act = $('#filter-brand').val();
    if (brand.checked) {
        $('#filter-brand').val(act + brand.value + ',')
    } else {
        $('#filter-brand').val(act.replace(brand.value + ',', ''))
    }
    var event = new Event('change');
    document.getElementById('filter-brand').dispatchEvent(event);
}

function buildCompaniesDropdown(companyList) {
    $("#span-comp").html("");
    companies = "";
    if (companyList.length > 0) {
        $.each(companyList, function(index, value) {
            companies +=
              "<li>" +
                  "<div class='ml-2 pretty p-default p-curve p-smooth'>" +
                      "<input id='" + value + "' value='" + value + "' type='checkbox' onchange='addCompany(this)' />" +
                      "<div class='state p-info'>" +
                          "<label>" + value + "</label>" +
                      "</div>" +
                  "</div>" +
              "</li>";
        });
    } else {
        companies +=
          "<li clasvalue label htmls='checkbox keep-open' class='text-center'>" +
              "<span class='text-muted text-center'>Sin tiendas</span>" +
          "</li>";
    }

    var html =
        "<span class='dropdown'>" +
            "<span class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>" +
                "<button id='btn-companies' class='btn btn-primary btn-sm mr-2 sear'>Tiendas</button>" +
            "</span>" +
            "<ul class='dropdown-menu' role='menu' style='max-height: 250px; overflow-y: auto;'>" +
                companies +
            "</ul>" +
        "</span>";
    $("#span-comp").html(html);
}

function buildBrandsDropdown(brandList) {
    $("#span-brands").html("");
    brands = "";
    blackList = ["-", "NO FOUND"]
    if (brandList.length > 0) {
        $.each(brandList, function(index, value) {
            if (blackList.indexOf(value) != -1) {
                return;
            }
            brands +=
              "<li>" +
                  "<div class='ml-2 pretty p-default p-curve p-smooth'>" +
                      "<input id='" + value + "' value='" + value + "' type='checkbox' onchange='addBrand(this)' />" +
                      "<div class='state p-info'>" +
                          "<label>" + value + "</label>" +
                      "</div>" +
                  "</div>" +
              "</li>";
        });
    } else {
        brands +=
          "<li clasvalue label htmls='checkbox keep-open' class='text-center'>" +
              "<span class='text-muted text-center'>Sin marcas</span>" +
          "</li>";
    }

    var html =
        "<span class='dropdown'>" +
            "<span class='dropdown-toggle' data-toggle='dropdown' aria-expanded='true'>" +
                "<button id='btn-companies' class='btn btn-primary btn-sm mr-2 sear'>Marcas</button>" +
            "</span>" +
            "<ul class='dropdown-menu' role='menu' style='max-height: 250px; overflow-y: auto;'>" +
                brands +
            "</ul>" +
        "</span>";
    $("#span-brands").html(html);
}
