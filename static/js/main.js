/********Animación Header*********/
var senseSpeed = 5;
var previousScroll = 0;
$(window).scroll(function (event) {
    var scroller = $(this).scrollTop();
    if (scroller - senseSpeed > previousScroll) {
        $("nav.navbar").filter(':not(:animated)').slideUp();
    } else if (scroller + senseSpeed < previousScroll) {
        $("nav.navbar").filter(':not(:animated)').slideDown();
    }
    previousScroll = scroller;
});

$("#left-slide").addClass("ui-state-hover");
$("#left-slide").addClass("ui-state-active");
$("#left-slide").addClass("ui-state-focus");

var mapData = [];
var route;

$(document).ready(function () {
    $("body").on("click", ".login-redirect", function (event) {
        event.preventDefault();
        var href = $(event.target).attr("href");
        var next = window.location.pathname + window.location.search;
        $.redirect(href, {"next": next}, "get");
    });
    $("#nav-form, #main-form").submit(function (event) {
        var term = $("#search_main").val().toUpperCase();
        $.redirect("/search/" + term, {}, "get");
        event.preventDefault();
    });
});

function copyCompareLink() {
    var coords = $("#coords").val();
    var productName = $(".actions-items").attr("data-product-name");
    var shareLink = window.location.href;

    navigator.permissions.query({name: "clipboard-write"}).then(result => {
        if (result.state == "granted" || result.state == "prompt") {
            navigator.clipboard.writeText(shareLink);
        } else {
            window.prompt("Copiar enlace:", shareLink);
        }
        $.notify({
                "message": '¡El enlace a este producto ha sido copiado!'
            },
            {
                "allow_dismiss": false,
                "mouse_over": "pause",
                "placement": {
                    "from": "bottom",
                    "align": "center"
                },
                "z_index": 1060,
                "type": "success",
            }
        );
    });

    // Save this event
    $.ajax({
        url: "/save-event",
        dataType: "json",
        method: "post",
        data: {
            coords: coords,
            type: "product:share-compare-link",
            product_name: productName
        },
    });
}

function shareCompareToFacebook() {
    var coords = $("#coords").val();
    var productName = $(".actions-items").attr("data-product-name");
    var shareLink = window.location.href;
    var url = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(shareLink) + '&quote=Revisa esta comparación de precios para el producto ' + productName;
    window.open(url, "_blank");

    // Save this event
    $.ajax({
        url: "/save-event",
        dataType: "json",
        method: "post",
        data: {
            coords: coords,
            type: "product:share-compare-facebook",
            product_name: productName
        },
    });
}

function shareCompareToTwitter() {
    var coords = $("#coords").val();
    var productName = $(".actions-items").attr("data-product-name");
    var shareLink = window.location.href;
    var url = 'https://twitter.com/share?text=Revisa esta comparación de precios para el producto ' + productName + ':&url=' + encodeURIComponent(shareLink) + '&via=prixtips&hashtags=prixtips,compara,precios,comprainteligente';
    window.open(url, "_blank");

    // Save this event
    $.ajax({
        url: "/save-event",
        dataType: "json",
        method: "post",
        data: {
            coords: coords,
            type: "product:share-compare-twitter",
            product_name: productName
        },
    });
}

function isMobile() {
    return /Mobi|Android/i.test(navigator.userAgent);
}

function showRoute(imageElement) {
    markersLayer.clearLayers();
    positionLayer.clearLayers();

    // Display route maps
    var coords = $("#coords").val();
    var distance = Math.pow(10, 30);
    var productName = $(imageElement).attr("data-product");
    var productCompany = $(imageElement).attr("data-company");

    removeNotifications();

    if (coords) {
        var coordsLatLng = coords.split(",");
        var locationLatLng = L.latLng(coordsLatLng[0], coordsLatLng[1]);
        var closestLatLng;

        for (var i = 0; i < mapData.length; i++) {
            var coordinates = mapData[i]["coordinates"];
            var iconUrl = mapData[i]["logo"];
            var title = mapData[i]["name"];
            var company = mapData[i]["company"]
            var html = mapData[i]["html"]

            if (productCompany != company) {
                continue
            }

            var customIcon = L.icon({
                iconUrl: iconUrl,
            });

            if (typeof coordinates !== "undefined") {
                var markerLatLng = L.latLng(coordinates[0], coordinates[1]);
                var distanceToLocation = markerLatLng.distanceTo(locationLatLng);
                if (distanceToLocation < distance) {
                    distance = distanceToLocation;
                    closestLatLng = markerLatLng;
                }
                newMark = L.marker(markerLatLng, {
                    icon: customIcon,
                    title: title
                })
                newMark.on('click', function (e) {
                    drawRoute(locationLatLng, this.getLatLng());
                });
                newMark.addTo(map).bindPopup(html);
                newMark.addTo(markersLayer);
            }
        }

        // Routing to nearest company
        if (locationLatLng && closestLatLng) {
            drawRoute(locationLatLng, closestLatLng);
        }

        // Save this event
        $.ajax({
            url: "/save-event",
            dataType: "json",
            method: "post",
            data: {
                coords: coords,
                type: "product:route",
                product_name: productName,
                product_company: productCompany
            },
        });

        $(".main-maps").show();
        map.invalidateSize(true);
        map.locate({setView: true, watch: false, enableHighAccuracy: false, maximumAge: 3000, maxZoom: 15});
    } else {
        $.notify({
                "message": 'No es posible usar el servicio de geolocalización. Compruebe la configuración de su explorador.'
            },
            {
                "allow_dismiss": false,
                "mouse_over": "pause",
                "placement": {
                    "from": "bottom",
                    "align": "center"
                },
                "z_index": 1060,
                "type": "warning",
            }
        );
    }

    if (isMobile()) {
        $(".list-result").hide();
        $(".main-maps").show();
    }
}

function removeNotifications() {
    $("div[data-notify=container]").remove();
}

function addProductToFavorites(event) {
//    var productName = $(event.target).parents("div .card-item, .actions-items").attr("data-product-name").replace(/"/g, "&quot;");
    var productName = $(event.target).parents(".resultBody__box__img__icons__tool").attr("data-product");
    if (productName == null) {
        productName = $(event.target).parents(".resultProduct__contentProduct__icons").attr("data-product");
    }

    var coords = $("#coords").val();

    // Hide all tooltips
    $("[data-toggle='tooltip']").tooltip("hide");

    $.ajax({
        url: "/add-product-to-favorites",
        dataType: "json",
        method: 'post',
        data: {
            coords: coords,
            product_name: productName,
        },
        success: function (response) {
            removeNotifications();
            if (response.added) {
                $(event.target).addClass("fa-heart text-danger").removeClass("fa-heart-o");
                $.notify({
                        "message": response.message
                    },
                    {
                        "allow_dismiss": false,
                        "mouse_over": "pause",
                        "placement": {
                            "from": "bottom",
                            "align": "center"
                        },
                        "z_index": 1060,
                        "type": "success",
                    }
                );
            } else {
                $(event.target).addClass("fa-heart-o").removeClass("fa-heart text-danger");
                $.notify({
                        "message": response.message
                    },
                    {
                        "allow_dismiss": false,
                        "mouse_over": "pause",
                        "placement": {
                            "from": "bottom",
                            "align": "center"
                        },
                        "z_index": 1060,
                        "type": "success",
                    }
                );
            }
        },
        error: function (response) {
            removeNotifications();
            $.notify({
                    "message": response.responseJSON.message
                },
                {
                    "allow_dismiss": false,
                    "mouse_over": "pause",
                    "placement": {
                        "from": "bottom",
                        "align": "center"
                    },
                    "z_index": 1060,
                    "type": response.responseJSON.type,
                }
            );
        }
    });
}


function getShoppingLists(event) {

    var productName = $(event.target).parents(".resultBody__box__img__icons__tool").attr("data-product");
    if (productName == null) {
        productName = $(event.target).parents(".resultProduct__contentProduct__icons").attr("data-product");
    }
    console.log(productName);

    // Hide all tooltips
    $("[data-toggle='tooltip']").tooltip("hide");

    $.ajax({
        url: "/shopping-lists",
        dataType: "json",
        method: "get",
        data: {
            product_name: productName
        },
        success: function (response) {
            var shoppingLists = response.shopping_lists;

            // Clear current items
            $("#shopping-list-modal #shopping-lists-container").html("");

            // Set modal title and input
            $("#shopping-list-modal .modal-title").html(productName);
            $("#shopping-list-modal #shopping-list-product-name").val(productName);

            // Reset create form
            $("#shopping-list-modal #shopping-list-toggle-btn").removeClass("d-none");
            $("#shopping-list-modal #create-shopping-list-form").addClass("d-none");

            // Display result
            $("#shopping-list-modal #shopping-lists-container").append(response.html);
            $("#shopping-list-modal").modal();
        },
        error: function (response) {
            removeNotifications();
            $.notify({
                    "message": response.responseJSON.message
                },
                {
                    "allow_dismiss": false,
                    "mouse_over": "pause",
                    "placement": {
                        "from": "bottom",
                        "align": "center"
                    },
                    "z_index": 1060,
                    "type": response.responseJSON.type,
                }
            );
        }
    });
}

function updateProductInShoppingList(event) {
    var term = $("#search_main").val();
    var coords = $("#coords").val();
    var productName = $("#shopping-list-modal #shopping-list-product-name").val();
    var shoppingListSlug = $(event.target).val();
    var add = $(event.target).is(":checked");

    $.ajax({
        url: "/update-product-in-shopping-list",
        dataType: "json",
        method: "post",
        data: {
            term: term,
            coords: coords,
            product_name: productName,
            shopping_list_slug: shoppingListSlug,
            add: add
        },
        success: function (response) {
            removeNotifications();
            if (add) {
                $.notify({
                        "message": response.message
                    },
                    {
                        "allow_dismiss": false,
                        "mouse_over": "pause",
                        "placement": {
                            "from": "bottom",
                            "align": "center"
                        },
                        "z_index": 1060,
                        "type": "success",
                    }
                );
            } else {
                $.notify({
                        "message": response.message
                    },
                    {
                        "allow_dismiss": false,
                        "mouse_over": "pause",
                        "placement": {
                            "from": "bottom",
                            "align": "center"
                        },
                        "z_index": 1060,
                        "type": "success",
                    }
                );
            }
        },
        error: function (response) {
            removeNotifications();
            $.notify({
                    "message": response.responseJSON.message
                },
                {
                    "allow_dismiss": false,
                    "mouse_over": "pause",
                    "placement": {
                        "from": "bottom",
                        "align": "center"
                    },
                    "z_index": 1060,
                    "type": response.responseJSON.type,
                }
            );
        }
    });
}

function customCompareProduct(event) {
//    console.log($(event.target).parents(".resultBody__box__img__icons__tool").attr("data-product"));
    var productName = $(event.target).parents(".resultBody__box__img__icons__tool").attr("data-product");
    var coords = $("#coords").val();

    // Hide all tooltips
    $("[data-toggle='tooltip']").tooltip("hide");

    if (selectedCompareProductsNames.indexOf(productName) == -1) {
        selectedCompareProductsNames.push(productName);
        selectedCompareProductsNames.sort();
    }
    var html = "<ul>";
    for (var i = 0; i < selectedCompareProductsNames.length; i++) {
        html += "<li>" + selectedCompareProductsNames[i] + "</li>";
    }
    html += "</ul>"
    $("#compare-product-list .list-container").html(html);
    $("#btn-custom-compare").removeClass("d-none");
    $("#btn-reset-compare").removeClass("d-none");
    $("#product-counter").html(selectedCompareProductsNames.length);
    removeNotifications();
    $.notify({
            "message": "El producto fue agregado a tu lista de comparación. Ahora tienes <strong>" + selectedCompareProductsNames.length + " producto(s)</strong>."
        },
        {
            "allow_dismiss": false,
            "mouse_over": "pause",
            "placement": {
                "from": "bottom",
                "align": "center"
            },
            "z_index": 1060,
            "type": "success",
        }
    );
    return false;
}

var min = document.getElementById('filter-min-price');
var max = document.getElementById('filter-max-price');

function changein() {
    document.getElementById('min-span').setAttribute('value', parseInt($('#filter-min-price').val()));
    document.getElementById('max-span').setAttribute('value', parseInt($('#filter-max-price').val()));

}

function changeLeft() {
    $("#min-span").val($('#filter-min-slider').val());
    $("#max-span").text($('#filter-max-slider').val());

}

function changeRigth() {
    $("#min-span").text($('#filter-min-slider').val());
    $("#max-span").text($('#filter-max-slider').val());

}

function ShowData(dataHtml, validate, min, max) {
    // if (!validate) {
    //     $("#resultBody").loading("stop");
    //     return;
    // }

    if (dataHtml) {
        $("#resultBody").html('');
        $("#resultBody").html(dataHtml);
        $(document).ready(function () {
            $(".owl-one").owlCarousel({
                loop: false,
                //margin: 10,
                navigation: false,
                //stagePadding: 32,
                //slideSpeed : 500,
                //paginationSpeed : 800,
                //rewindSpeed : 1000,
                //singleItem: true,
                //stopOnHover : true,
                ///autoplay: true,
                slideTransition: 'linear',
                autoplayTimeout: 2000,
                autoplaySpeed: 2000,
                //smartSpeed: 1000,
                autoplayHoverPause: false,
                rtl: false,
                dots: false,
                nav: false,
                navText: [
                    "atrás",
                    "siguiente"],
                responsive: {
                    0: {
                        items: 2
                    },
                    600: {
                        items: 5
                    },
                    1000: {
                        items: 4
                    }
                }
            });
        });
    } else {
        var html =
            "<div class='text-center mt-3'>" +
            "<h2 class='text-muted'>" +
            "¡Oops!" +
            "</h2>" +
            "<div class='text-muted mb-3'>" +
            "<i class='fa fa-cart-arrow-down fa-4x'></i>" +
            "</div>" +
            "<div class='text-muted lead'>" +
            "No hemos encontrado precios actualizados al día de hoy para lo que buscas." +
            "</div>" +
            "<div class='text-muted lead'>" +
            "¿Te gustaría ver este producto? Puedes solicitarlo usando nuestro <a href='/support'>formulario de soporte</a>." +
            "</div>" +
            "</div>";
        $("#resultBody").append("<div id='product-list'>" + html + "</div>");
    }


}

var compareTable = null;

function ShowCompareModal(dataSet) {
    if (compareTable != null) {
        $(compareTable).DataTable().destroy();
        $("#compare-modal .modal-body .dataTables_wrapper").remove();
    }

    $("#compare-modal .modal-table-container").append(
        '<table class="table table-bordered" style="width: 100%;"></table>'
    );

    compareTable = $("#compare-modal table").DataTable({
        destroy: true,
        bSort: false,
        header: true,
        footer: true,
        paging: false,
        searching: false,
        info: false,
        data: dataSet.rows,
        columns: dataSet.columns,
        language: {
            "lengthMenu": "Mostrar _MENU_ resultados",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(Filtrados de _MAX_ registros totales)",
            "search": "Filtrar:",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        drawCallback: function (settings) {
            var template = "<div class='tooltip' style='opacity: 1 !important' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>";
            $('[data-toggle="tooltip"]').tooltip({template: template});
        },
    });

    $("#compare-modal").modal();
}

function ShowCompareDetail(dataSet) {
    compareTable = $("#results-container table").DataTable({
        destroy: true,
        bSort: false,
        header: true,
        footer: true,
        paging: false,
        searching: false,
        info: false,
        data: dataSet.rows,
        columns: dataSet.columns,
        language: {
            "lengthMenu": "Mostrar _MENU_ resultados",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(Filtrados de _MAX_ registros totales)",
            "search": "Filtrar:",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        drawCallback: function (settings) {
            var template = "<div class='tooltip' style='opacity: 1 !important' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>";
            $('[data-toggle="tooltip"]').tooltip({template: template});
            $("table thead").remove();
        },
    });

    // Load the image in the container, look for a valid one
    var imageUrl = "/static/img/invalid-product.png";
    $(dataSet.images).each(function (index, value) {
        if (value && value.indexOf("base64") == -1) {
            imageUrl = value;
            return false;
        }
    });
    var imageHtml = "<img src='" + imageUrl + "' class='img-thumbnail' width='300' onerror='this.onerror=null;this.src=\"/static/img/invalid-product.png\"'>";
    $("#product-detail .image-container").html(imageHtml);

    // Show the container
    $("#results-container").removeClass("d-none");
}

function Searcher(term, pag) {
    if (term) {
        // $("#result-page").loading({
        //     message: "Cargando los últimos precios <div class='mt-2'><i class='fa fa-spinner fa-spin fa-3x fa-fw'></i></div>",
        //     theme: "light",
        //     stoppable: false,
        // });
        $.ajax({
            url: "/api/search",
            dataType: "json",
            data: {
                term: term,
                pag: pag
            },
            success: function (status) {
                var fixedTerm = status["term"];
                var validate = status["validate"];
                if (status["code"] == 200) {
                    var html = status["html"];
                    var companies = status["companies"];
                    var brands = status["brands"];
                    var minPrice = status["min_price"];
                    var maxPrice = status["max_price"];
                    var term = status["term"];
                    ///////////////////////////////////
                    var num_page = status["num_pages"];

                    var cur_page = status['current_page'];
                    console.log(cur_page)
                    //////////////////////////////////

                    // BUILD PAGE
                    build_page(num_page, cur_page);

                    // Display new data
                    ShowData(html, validate, minPrice, maxPrice);

                    // Clear companies filter
                    load_store(companies);

                    // Build slide and dropdowns
                    load_brands(brands);

                    // Build min and max prices
                    load_range_prices(minPrice, maxPrice);


                    getFavoriteSearches();
                } else if (status["code"] == 400) {
                    var html =
                        "<div class='text-center mt-3'>" +
                        "<h2 class='text-muted'>" +
                        "¡Oops!" +
                        "</h2>" +
                        "<div class='text-muted mb-3'>" +
                        "<i class='fa fa-cart-arrow-down fa-4x'></i>" +
                        "</div>" +
                        "<div class='text-muted lead'>" +
                        "Intenta ampliar el término de tu búsqueda para poder entregarte los mejores resultados." +
                        "</div>" +
                        "</div>";
                    $("#resultBody").append("<div id='product-list'>" + html + "</div>");

                } else if (status["code"] == 500) {
                    var html =
                        "<div class='text-center mt-3'>" +
                        "<h2 class='text-muted'>" +
                        "¡Oops!" +
                        "</h2>" +
                        "<div class='text-muted mb-3'>" +
                        "<i class='fa fa-cart-arrow-down fa-4x'></i>" +
                        "</div>" +
                        "<div class='text-muted lead'>" +
                        "Tu búsqueda no ha podido ser completada. Estamos trabajando para solucionarlo." +
                        "</div>" +
                        "</div>";
                    $("#product-area").append("<div id='product-list'>" + html + "</div>");
                    $("#result-page").loading("stop");
                }

                // Fill search input with current term
                $("#search_main").val(fixedTerm || "");
            },
            error: function (response) {
                var html =
                    "<div class='text-center mt-3'>" +
                    "<h2 class='text-muted'>" +
                    "¡Oops!" +
                    "</h2>" +
                    "<div class='text-muted mb-3'>" +
                    "<i class='fa fa-cart-arrow-down fa-4x'></i>" +
                    "</div>" +
                    "<div class='text-muted lead'>" +
                    "Tu búsqueda no ha podido ser completada. Estamos trabajando para solucionarlo." +
                    "</div>" +
                    "</div>";
                $("#product-area").append("<div id='product-list'>" + html + "</div>");
                $("#result-page").loading("stop");
            }
        });
    }
}

function Comparer(productNames, coords, single) {
    if (typeof productNames == "string") {
        productNames = [productNames]
    }

//    console.log(productNames)

    $.ajax({
        url: "/compare-products",
        dataType: "json",
        data: {
            coords: coords,
            products: productNames,
            single: single
        },
        success: function (status) {
            var validate = status['validate'];
            var dataSet = status['data'];
            if (status['code'] == 200) {
                if (single == false) {
                    console.log(dataSet);
                } else {
//                    var nameProduct = dataSet[0][0];
                    var title = $("h2.resultProduct__title--txt");
                    title.html("");
                    title.html(productNames[0]);
                    var imagesProduct = dataSet['images'][0];
                    if (imagesProduct != null) {
                        imagesProduct = imagesProduct.replace('http:', 'https:')
                    }
                    var img = $("img#compareProductImage");
                    img.attr('src', imagesProduct);
                    var stores = $("div#contentBrandsPrice");
                    var prices = [];
                    stores.html('');
                    for (let x of dataSet['rows']) {
                        prices.push(parseFloat(x[3]));
                        col = `
                    <div class="resultProduct__contentBrands__box">
                            <div class="resultProduct__contentBrands__boxContent">

                                <div class="resultProduct__contentBrands__box__grid">
                                    <div class="resultProduct__contentBrands__boxInt">
                                        <div class="resultProduct__contentBrands__boxInt__img">
                                            <div class="resultProduct__contentBrands__boxInt__imgInt">
                                                <img style="cursor: pointer;" src=${x[2]} alt="" class="resultProduct__contentBrands__boxInt__imgInt--src" data-company=${x[1]} onclick="showRoute(this)" data-product="${x[0]}">
                                            </div>
                                        </div>
                                        <!-- <a href="#" class="resultProduct__contentBrands__boxInt__common">
                                            <p class="resultProduct__contentBrands__boxInt__common--txt">Ver ubicación</p>
                                        </a> -->
                                    </div>
                                </div>
                                <div class="resultProduct__contentBrands__box__grid">
                                    <div class="resultProduct__contentBrands__boxInt">
                                        <div class="resultProduct__contentBrands__boxInt__price">
                                            <p class="resultProduct__contentBrands__boxInt__price--txt">S/ ${x[3]}</p>
                                        </div>
                                        <!-- <a  href="#"class="resultProduct__contentBrands__boxInt__common">
                                            <p href="#" class="resultProduct__contentBrands__boxInt__common--txt">Comprar</p>
                                        </a> -->
                                    </div>
                                </div>
                                <div class="resultProduct__contentBrands__box__grid">
                                    <div class="resultProduct__contentBrands__boxInt">
                                        <div class="resultProduct__contentBrands__boxInt__offer">
                                            <p class="resultProduct__contentBrands__boxInt__offer--txt">${x[5]}</p>
                                        </div>
                                        <!-- <div class="resultProduct__contentBrands__boxInt__common">
                                            <p class="resultProduct__contentBrands__boxInt__common--txt resultProduct__contentBrands__boxInt__common--txtAnimatee">Restricciones</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="resultProduct__contentBrands__box__bottom">
                                <div class="resultProduct__contentBrands__box__bottom__grid">
                                    <a href="#" class="resultProduct__contentBrands__box__bottom__grid__common">
                                        <p class="resultProduct__contentBrands__box__bottom__grid__common--txt">Ver ubicación</p>
                                    </a>
                                </div>
                                <div class="resultProduct__contentBrands__box__bottom__grid">
                                    <a href="#" class="resultProduct__contentBrands__box__bottom__grid__common">
                                        <p class="resultProduct__contentBrands__box__bottom__grid__common--txt">Comprar</p>
                                    </a>
                                </div>
                                <div class="resultProduct__contentBrands__box__bottom__grid">
                                    <div href="#" class="resultProduct__contentBrands__box__bottom__grid__common">
                                        <p class="resultProduct__contentBrands__box__bottom__grid__common--txt">Restricciones</p>
                                        <div class="resultProduct__contentBrands__box__bottom__grid__common__restrict" style="display: none">
                                            <div class="resultProduct__contentBrands__box__bottom__grid__common__restrictBox">
                                                <p class="resultProduct__contentBrands__box__bottom__grid__common__restrictBox--txt">Válido a partir de 4 productos</p>
                                            </div>
                                        </div>
                                        <!-- <p class="resultProduct__contentBrands__box__bottom__grid__common--tooltip"></p> -->
                                    </div>
                                </div>
                            </div>
                            <!-- <p class="resultProduct__contentBrands__box__text">* Válido a partir de  4 productos</p> -->
                        </div>
                    `
                        act_html = stores.html();
                        stores.html(act_html + col);
                    }
                    prices.sort();
                    var porc = parseInt(((prices[prices.length - 1] - prices[0]) / prices[prices.length - 1]) * 100.0);
                    if (porc > 0){
                        var desc = '<p class="resultProduct__contentProduct__offer__percen"><span class="resultProduct__contentProduct__offer__percen--txt">Ahorra hasta &nbsp;</span><span class="resultProduct__contentProduct__offer__percen--number">'+ porc +'%</span></p>';
                    }else{
                        var desc = ''
                    }

                    var porcent = $('div.resultProduct__contentProduct__offer');
                    porcent.html('');
                    porcent.html(desc);
                    var img_product = $('img#img_product');
                    img_product.attr('src', dataSet['images'][0])
                }

                if (single == true) {
                    if ($.isEmptyObject(dataSet)) {
                        $.redirect("/ ", {}, "get");
                    } else {
                        ShowCompareDetail(dataSet);
                    }
                } else {
                    ShowCompareModal(dataSet);
                }
            }
        }
    });
}

function drawRoute(origin, destination) {
    if (route) {
        map.removeControl(route);
    }

    var waypoints = [origin, destination];
    route = L.Routing.control({
        serviceUrl: "https://sitak.prix.tips/route/v1",
        waypoints: waypoints,
        lineOptions: {
            styles: [
                {
                    color: 'black',
                    opacity: 0.15,
                    weight: 9
                },
                {
                    color: 'white',
                    opacity: 0.8,
                    weight: 6
                },
                {
                    color: '#DC143C',
                    opacity: 1,
                    weight: 4
                }
            ]
        },
        createMarker: function (i, waypoints, n) {
            return;
        },
        routeWhileDragging: false,
        show: false,
        addWaypoints: false,
        draggableWaypoints: false
    });
    route.addTo(map);
}

function updateFavoriteSearch(term, remove) {
    var coords = $("#coords").val();
    var url = "/add-search-to-favorites"
    if (remove) {
        url = "/remove-search-from-favorites"
    }

    $.ajax({
        url: url,
        method: "post",
        dataType: "json",
        data: {
            coords: coords,
            term: term
        },
        success: function (response) {
            removeNotifications();

            // Update searches list
            updateUserSearchList(response.searches);

            $.notify({
                    "message": response.message
                },
                {
                    "allow_dismiss": false,
                    "mouse_over": "pause",
                    "placement": {
                        "from": "bottom",
                        "align": "center"
                    },
                    "z_index": 1060,
                    "type": "success",
                }
            );
        },
        error: function (response) {
            removeNotifications();
            $.notify({
                    "message": response.responseJSON.message
                },
                {
                    "allow_dismiss": false,
                    "mouse_over": "pause",
                    "placement": {
                        "from": "bottom",
                        "align": "center"
                    },
                    "z_index": 1060,
                    "type": response.responseJSON.type,
                }
            );
        }
    });
}

function getFavoriteSearches() {
    $.ajax({
        url: "/favorite-searches",
        dataType: "json",
        method: "get",
        data: {},
        success: function (searches) {
            // Populate custom searches
            var currentTerm = $("#search_main").val();
            updateUserSearchList(searches);
        }
    });
}

function searchFromFavorite(element) {
    var term = $(element).attr("data-term");
    $.redirect("/search/" + term, {}, "get");
}

function buyProduct(event) {
    var productName = $(event.target).parents(".modal").find("div[data-jplist-item]").attr("data-product-name");
    var productCompany = $(event.target).attr("data-company");
    var coords = $("#coords").val();
    var session = $("#session").val();

    if (!session) {
        event.preventDefault();
        removeNotifications();
        $.notify({
                "message": "Para comprar productos debes <a class='login-redirect' href='/account/login'>iniciar sesión</a>. ¿No tienes una cuenta? ¡<a href='/account/register'>Regístrate</a>! Es gratis."
            },
            {
                "allow_dismiss": false,
                "mouse_over": "pause",
                "placement": {
                    "from": "bottom",
                    "align": "center"
                },
                "z_index": 1060,
                "type": "warning",
            }
        );
    }

    // Save this event
    $.ajax({
        url: "/save-event",
        dataType: "json",
        method: "post",
        data: {
            coords: coords,
            type: "product:buy",
            product_name: productName,
            product_company: productCompany
        },
    });
}

function updateUserSearchList(searches) {
    var currentTerm = $("#search_main").val();
    var htmlSearches = "<ul class='list-unstyled'>";

    $(searches).each(function (index, value) {
        htmlSearches += "" +
            "<li onclick='searchFromFavorite(this)' data-term='" + value.term + "' style='color: #6C707C; cursor: pointer; margin-bottom: 7px;'>" +
            "<i class='text-warning fa fa-star fa-fw'></i>" +
            value.term
        "</li>";
    });
    htmlSearches += "</ul>"
    if (searches.length > 0) {
        $("#my-searches").html(htmlSearches);
    } else {
        $("#my-searches").html("<p style='color: #6C707C;'>No has guardado ninguna búsqueda</p>");
    }
}


function getMapData(origin) {
    $.ajax({
        url: "/map-data",
        dataType: "json",
        method: "get",
        data: {
            origin: origin
        },
        success: function (response) {
            mapData = response.data;
        }
    });
}


// Product trends

function displayTrends() {
    $.ajax({
        url: "/api/product-trends",
        success: function (response) {
            var html = "<div class='row'>";
            var elements = response.trends.slice(0, 6);
            var perRow = 3;
            $(elements).each(function (index, element) {
                html +=
                    "<div class='col text-center'>" +
                    "<img src='" + element.image + "' class='card-img-top' alt='" + element.name.toUpperCase() + "' style='width: 100px!important; cursor: pointer;' data-trend=" + element.name.toUpperCase() + " onclick='searchFromTrend(this);'>" +
                    "<div class='mt-2'>" +
                    "<p class='small font-weight-bold' style='color: #16CDCD;'>" + element.name.toUpperCase() + "</p>" +
                    "</div>" +
                    "</div>";
                if ((index + 1) % perRow == 0) {
                    html += "</div><div class='row'>";
                }
            });
            html += "</div>"
            $("#trends").html(html);
        },
    });
}

function searchFromTrend(element) {
    var trendTerm = $(element).attr("data-trend");

    // Save this event
    $.ajax({
        url: "/save-event",
        dataType: "json",
        method: "post",
        data: {
            type: "feature:trend",
            search_term: trendTerm
        },
    });

    $.redirect("/search/" + trendTerm, {}, "get");
}
