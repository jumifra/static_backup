/********Animación Header*********/
 var senseSpeed = 5;
 var previousScroll = 0;
 $(window).scroll(function(event){
    var scroller = $(this).scrollTop();
    if (scroller-senseSpeed > previousScroll){
       $("nav.navbar").filter(':not(:animated)').slideUp();
    } else if (scroller+senseSpeed < previousScroll) {
       $("nav.navbar").filter(':not(:animated)').slideDown();
    }
    previousScroll = scroller;
 });

$("#left-slide" ).addClass( "ui-state-hover" );
$("#left-slide" ).addClass( "ui-state-active" );
$("#left-slide" ).addClass( "ui-state-focus" );

var mapData = [];
var route;


/**************svg-img.js en carperta js****************/
$("img.svg-icon").each(function () {
    // Perf tip: Cache the image as jQuery object so that we don't use the selector muliple times.
    var $img = jQuery(this);
    // Get all the attributes.
    var attributes = $img.prop("attributes");
    // Get the image's URL.
    var imgURL = $img.attr("src");
    // Fire an AJAX GET request to the URL.
    $.get(imgURL, function (data) {
        // The data you get includes the document type definition, which we don't need.
        // We are only interested in the <svg> tag inside that.
        var $svg = $(data).find('svg');
        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');
        // Loop through original image's attributes and apply on SVG
        $.each(attributes, function() {
            $svg.attr(this.name, this.value);
        });
        // Replace image with new SVG
        $img.replaceWith($svg);
    });
});


/******************************/

$(document).ready(function() {
    $("body").on("click", ".login-redirect", function(event) {
        event.preventDefault();
        var href = $(event.target).attr("href");
        var next = window.location.pathname + window.location.search;
        $.redirect(href, {"next": next}, "get");
    });
    $("#nav-form, #main-form").submit(function (event) {
        var term = $("#search_main").val().toUpperCase();
        $.redirect("/compare", {"term": term, "custom": true}, "get");
        event.preventDefault();
    });
});

function copyCompareLink() {
    var coords = $("#coords").val();
    var productName = $(".actions-items").attr("data-product-name");
    var shareLink = window.location.href;

    navigator.permissions.query({name: "clipboard-write"}).then(result => {
        if (result.state == "granted" || result.state == "prompt") {
            navigator.clipboard.writeText(shareLink);
        } else {
            window.prompt("Copiar enlace:", shareLink);
        }
        $.notify({
                "message": '¡El enlace a este producto ha sido copiado!'
            },
            {
                "allow_dismiss": false,
                "mouse_over": "pause",
                "placement": {
                    "from": "bottom",
                    "align": "center"
                },
                "z_index": 1060,
                "type": "success",
            }
        );
    });

    // Save this event
    $.ajax({
        url: "/save-event",
        dataType: "json",
        method: "post",
        data: {
            coords: coords,
            type: "product:share-compare-link",
            product_name: productName
        },
    });
}

function shareCompareToFacebook() {
    var coords = $("#coords").val();
    var productName = $(".actions-items").attr("data-product-name");
    var shareLink = window.location.href;
    var url = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(shareLink) + '&quote=Revisa esta comparación de precios para el producto ' + productName;
    window.open(url, "_blank");

    // Save this event
    $.ajax({
        url: "/save-event",
        dataType: "json",
        method: "post",
        data: {
            coords: coords,
            type: "product:share-compare-facebook",
            product_name: productName
        },
    });
}

function shareCompareToTwitter() {
    var coords = $("#coords").val();
    var productName = $(".actions-items").attr("data-product-name");
    var shareLink = window.location.href;
    var url = 'https://twitter.com/share?text=Revisa esta comparación de precios para el producto ' + productName + ':&url=' + encodeURIComponent(shareLink) + '&via=prixtips&hashtags=prixtips,compara,precios,comprainteligente';
    window.open(url, "_blank");

    // Save this event
    $.ajax({
        url: "/save-event",
        dataType: "json",
        method: "post",
        data: {
            coords: coords,
            type: "product:share-compare-twitter",
            product_name: productName
        },
    });
}

function isMobile() {
    return /Mobi|Android/i.test(navigator.userAgent);
}

function showRoute(imageElement) {
    markersLayer.clearLayers();
    positionLayer.clearLayers();

    // Display route maps
    var coords = $("#coords").val();
    var distance = Math.pow(10, 30);
    var productName = $(imageElement).attr("data-product");
    var productCompany = $(imageElement).attr("data-company");

    removeNotifications();

    if (coords) {
        var coordsLatLng = coords.split(",");
        var locationLatLng = L.latLng(coordsLatLng[0], coordsLatLng[1]);
        var closestLatLng;

        for (var i = 0; i < mapData.length; i++) {
            var coordinates = mapData[i]["coordinates"];
            var iconUrl = mapData[i]["logo"];
            var title = mapData[i]["name"];
            var company = mapData[i]["company"]
            var html = mapData[i]["html"]

            if (productCompany != company) {
                continue
            }

            var customIcon = L.icon({
                iconUrl: iconUrl,
            });

            if (typeof coordinates !== "undefined") {
                var markerLatLng = L.latLng(coordinates[0], coordinates[1]);
                var distanceToLocation = markerLatLng.distanceTo(locationLatLng);
                if (distanceToLocation < distance) {
                    distance = distanceToLocation;
                    closestLatLng = markerLatLng;
                }
                newMark = L.marker(markerLatLng, {
                    icon: customIcon,
                    title: title
                })
                newMark.on('click', function (e) {
                    drawRoute(locationLatLng, this.getLatLng());
                });
                newMark.addTo(map).bindPopup(html);
                newMark.addTo(markersLayer);
            }
        }

        // Routing to nearest company
        if (locationLatLng && closestLatLng) {
            drawRoute(locationLatLng, closestLatLng);
        }

        // Save this event
        $.ajax({
            url: "/save-event",
            dataType: "json",
            method: "post",
            data: {
                coords: coords,
                type: "product:route",
                product_name: productName,
                product_company: productCompany
            },
        });

        $(".main-maps").show();
        map.invalidateSize(true);
        map.locate({setView: true, watch: false, enableHighAccuracy: false, maximumAge: 3000, maxZoom: 15});
    } else {
        $.notify({
                "message": 'No es posible usar el servicio de geolocalización. Compruebe la configuración de su explorador.'
            },
            {
                "allow_dismiss": false,
                "mouse_over": "pause",
                "placement": {
                    "from": "bottom",
                    "align": "center"
                },
                "z_index": 1060,
                "type": "warning",
            }
        );
    }

    if (isMobile()) {
        $(".list-result").hide();
        $(".main-maps").show();
    }
}

function removeNotifications() {
    $("div[data-notify=container]").remove();
}

function addProductToFavorites(event) {
    var productName = $(event.target).parents("div .card-item, .actions-items").attr("data-product-name").replace(/"/g, "&quot;");
    var coords = $("#coords").val();

    // Hide all tooltips
    $("[data-toggle='tooltip']").tooltip("hide");

    $.ajax({
        url: "/add-product-to-favorites",
        dataType: "json",
        method: 'post',
        data: {
            coords: coords,
            product_name: productName,
        },
        success: function (response) {
            removeNotifications();
            if (response.added) {
                $(event.target).addClass("fa-heart text-danger").removeClass("fa-heart-o text-muted");
                $.notify({
                        "message": response.message
                    },
                    {
                        "allow_dismiss": false,
                        "mouse_over": "pause",
                        "placement": {
                            "from": "bottom",
                            "align": "center"
                        },
                        "z_index": 1060,
                        "type": "success",
                    }
                );
            } else {
                $(event.target).addClass("fa-heart-o text-muted").removeClass("fa-heart text-danger");
                $.notify({
                        "message": response.message
                    },
                    {
                        "allow_dismiss": false,
                        "mouse_over": "pause",
                        "placement": {
                            "from": "bottom",
                            "align": "center"
                        },
                        "z_index": 1060,
                        "type": "success",
                    }
                );
            }
        },
        error: function (response) {
            removeNotifications();
            $.notify({
                    "message": response.responseJSON.message
                },
                {
                    "allow_dismiss": false,
                    "mouse_over": "pause",
                    "placement": {
                        "from": "bottom",
                        "align": "center"
                    },
                    "z_index": 1060,
                    "type": response.responseJSON.type,
                }
            );
        }
    });
}


function getShoppingLists(event) {
    var productName = $(event.target).parents("div .card-item, .actions-items").attr("data-product-name");

    // Hide all tooltips
    $("[data-toggle='tooltip']").tooltip("hide");

    $.ajax({
        url: "/shopping-lists",
        dataType: "json",
        method: "get",
        data: {
            product_name: productName
        },
        success: function (response) {
            var shoppingLists = response.shopping_lists;

            // Clear current items
            $("#shopping-list-modal #shopping-lists-container").html("");

            // Set modal title and input
            $("#shopping-list-modal .modal-title").html(productName);
            $("#shopping-list-modal #shopping-list-product-name").val(productName);

            // Reset create form
            $("#shopping-list-modal #shopping-list-toggle-btn").removeClass("d-none");
            $("#shopping-list-modal #create-shopping-list-form").addClass("d-none");

            // Display result
            $("#shopping-list-modal #shopping-lists-container").append(response.html);
            $("#shopping-list-modal").modal();
        },
        error: function (response) {
            removeNotifications();
            $.notify({
                    "message": response.responseJSON.message
                },
                {
                    "allow_dismiss": false,
                    "mouse_over": "pause",
                    "placement": {
                        "from": "bottom",
                        "align": "center"
                    },
                    "z_index": 1060,
                    "type": response.responseJSON.type,
                }
            );
        }
    });
}

function updateProductInShoppingList(event) {
    var term = $("#search_main").val();
    var coords = $("#coords").val();
    var productName = $("#shopping-list-modal #shopping-list-product-name").val();
    var shoppingListSlug = $(event.target).val();
    var add = $(event.target).is(":checked");

    $.ajax({
        url: "/update-product-in-shopping-list",
        dataType: "json",
        method: "post",
        data: {
            term: term,
            coords: coords,
            product_name: productName,
            shopping_list_slug: shoppingListSlug,
            add: add
        },
        success: function (response) {
            removeNotifications();
            if (add) {
                $.notify({
                        "message": response.message
                    },
                    {
                        "allow_dismiss": false,
                        "mouse_over": "pause",
                        "placement": {
                            "from": "bottom",
                            "align": "center"
                        },
                        "z_index": 1060,
                        "type": "success",
                    }
                );
            } else {
                $.notify({
                        "message": response.message
                    },
                    {
                        "allow_dismiss": false,
                        "mouse_over": "pause",
                        "placement": {
                            "from": "bottom",
                            "align": "center"
                        },
                        "z_index": 1060,
                        "type": "success",
                    }
                );
            }
        },
        error: function (response) {
            removeNotifications();
            $.notify({
                    "message": response.responseJSON.message
                },
                {
                    "allow_dismiss": false,
                    "mouse_over": "pause",
                    "placement": {
                        "from": "bottom",
                        "align": "center"
                    },
                    "z_index": 1060,
                    "type": response.responseJSON.type,
                }
            );
        }
    });
}

function customCompareProduct(event) {
    var productName = $(event.target).parents("div .card-item").attr("data-product-name");
    var coords = $("#coords").val();

    // Hide all tooltips
    $("[data-toggle='tooltip']").tooltip("hide");

    if (selectedCompareProductsNames.indexOf(productName) == -1) {
        selectedCompareProductsNames.push(productName);
        selectedCompareProductsNames.sort();
    }
    var html = "<ul>";
    for (var i = 0; i < selectedCompareProductsNames.length; i++) {
        html += "<li>" + selectedCompareProductsNames[i] + "</li>";
    }
    html += "</ul>"
    $("#compare-product-list .list-container").html(html);
    $("#btn-custom-compare").removeClass("d-none");
    $("#btn-reset-compare").removeClass("d-none");
    $("#product-counter").html(selectedCompareProductsNames.length);
    removeNotifications();
    $.notify({
            "message": "El producto fue agregado a tu lista de comparación. Ahora tienes <strong>" + selectedCompareProductsNames.length + " producto(s)</strong>."
        },
        {
            "allow_dismiss": false,
            "mouse_over": "pause",
            "placement": {
                "from": "bottom",
                "align": "center"
            },
            "z_index": 1060,
            "type": "success",
        }
    );
    return false;
}

var min = document.getElementById('filter-min-price');
var max = document.getElementById('filter-max-price');

function changein() {
    document.getElementById('min-span').setAttribute('value', parseInt($('#filter-min-price').val()));
    document.getElementById('max-span').setAttribute('value', parseInt($('#filter-max-price').val()));

}

function changeLeft() {
    $("#min-span").val($('#filter-min-slider').val());
    $("#max-span").text($('#filter-max-slider').val());

}

function changeRigth() {
    $("#min-span").text($('#filter-min-slider').val());
    $("#max-span").text($('#filter-max-slider').val());

}

function ShowData(dataHtml, validate) {
    if (!validate) {
        $("#result-page").loading("stop");
        return;
    }

    $("#product-area").html("");
    if (dataHtml) {
        $("#product-area").append("<div id='product-list' class='layout-grid' data-jplist-group='group1'>" + dataHtml + "</div>");
        var table = $("#product-list").bind("dynatable:afterProcess", function (e, dynatable) {
            // Load tooltips
            var template = "<div class='tooltip' style='opacity: 1 !important' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>";
            $('[data-toggle="tooltip"]').tooltip({template: template});
        }).bind("dynatable:init", function(e, dynatable) {
            dynatable.queries.functions["min-price"] = function (record, queryValue) {
                var minSlider = $("#filter-min-slider").val();
                var maxSlider = $("#filter-max-slider").val();
                var minPrice = $("#min-price").text();
                var maxPrice = $("#max-price").text();
                if ((minPrice == NaN && maxSlider == NaN) || (minPrice == 0 && maxPrice == 0)) {
                    return parseFloat(record.min) >= parseFloat(0) && parseFloat(record.min) <= parseFloat(100000000);
                } else {
                    return parseFloat(record.min) >= parseFloat(minPrice.replace('S/ ', '')) && parseFloat(record.min) <= parseFloat(maxPrice.replace('S/ ', ''));
                }
            };
            dynatable.queries.functions["max-price"] = function (record, queryValue) {
                var minSlider = $("#filter-min-slider").val();
                var maxSlider = $("#filter-max-slider").val();
                var minPrice = $("#min-price").text();
                var maxPrice = $("#max-price").text();
                if ((minPrice == NaN && maxSlider == NaN) || (minPrice == 0 && maxPrice == 0)) {
                    return parseFloat(record.min) >= parseFloat(0) && parseFloat(record.min) <= parseFloat(100000000);
                } else {
                    return parseFloat(record.min) >= parseFloat(minPrice.replace('S/ ', '')) && parseFloat(record.min) <= parseFloat(maxPrice.replace('S/ ', ''));
                }
            };
            dynatable.queries.functions["company"] = function (record, queryValue) {
                var values = $("#filter-company").val();
                var substring = values.substring(0, values.length - 1);
                var arrayCompanies = substring.split(',');
                for (x = 0; x < arrayCompanies.length; x++) {
                    if (record.companies.includes(arrayCompanies[x])) {
                        return true;
                    }
                }
                return false;
            };
            dynatable.queries.functions["brand"] = function (record, queryValue) {
                var values = $("#filter-brand").val();
                var substring = values.substring(0, values.length - 1);
                var arrayBrands = substring.split(',');
                for (x = 0; x < arrayBrands.length; x++) {
                    if (record.brand.includes(arrayBrands[x])) {
                        return true;
                    }
                }
                return false;
            };
            dynatable.queries.functions["name"] = function (record, queryValue) {
              var name = $('#filter-product').val();
              if (name != '' && name != NaN) {
                  var arrayName = name.split(' ');
                  for (y = 0; y < arrayName.length; y++) {
                      if (record.name.indexOf(arrayName[y].toUpperCase()) != -1) {
                          return true;
                      }
                  }
              }
              return false;
            };
        }).dynatable({
            features: {
                search: false,
                perPageSelect: false,
                pushState: true,
                recordCount: isMobile() ? false : true
            },
            table: {
                bodyRowSelector: ".card"
            },
            dataset: {
                perPageDefault: 40,
                perPageOptions: [20, 40, 80]
            },
            writers: {
                _rowWriter: function (rowIndex, record, columns, cellWriter) {
                    return record.html;
                }
            },
            readers: {
                _rowReader: function (index, div, record) {
                    var html = $(div).get(0).outerHTML;
                    var pric = $(div).find('.min-price').text();
                    var mode = $(div).find('.mod-pro').text();
                    var name = $(div).find('.name').text();
                    var brand = $(div).attr("data-brand-name");
                    record.companies = [];
                    $(div).find('.product-companies img').each(function () {
                        record.companies.push($(this).data('company-name'))
                    });
                    record.html = html;
                    record.min = pric;
                    record.mod = mode;
                    record.brand = brand;
                    record.name = name;
                }
            },
            params: {
                records: "productos"
            },
            inputs: {
                queries: $(".sear")
            }
        });

        // Stuff to do for mobile devices
        if (isMobile()) {
            $("nav").removeClass("fixed-top");
            $("#product-list").addClass("text-center").removeClass("layout-grid");
            $(".custom-compare-product").addClass("d-none");
        }
    } else {
        var html =
            "<div class='text-center mt-3'>" +
                "<h2 class='text-muted'>" +
                    "¡Oops!" +
                "</h2>" +
                "<div class='text-muted mb-3'>" +
                    "<i class='fa fa-cart-arrow-down fa-4x'></i>" +
                "</div>" +
                "<div class='text-muted lead'>" +
                    "No hemos encontrado precios actualizados al día de hoy para lo que buscas." +
                "</div>" +
                "<div class='text-muted lead'>" +
                    "¿Te gustaría ver este producto? Puedes solicitarlo usando nuestro <a href='/support'>formulario de soporte</a>." +
                "</div>" +
            "</div>";
        $("#product-area").append("<div id='product-list'>" + html + "</div>");
    }

    // Stop loading control
    $("#result-page").loading("stop");
}

var compareTable = null;

function ShowCompareModal(dataSet) {
    if (compareTable != null) {
        $(compareTable).DataTable().destroy();
        $("#compare-modal .modal-body .dataTables_wrapper").remove();
    }

    $("#compare-modal .modal-table-container").append(
        '<table class="table table-bordered" style="width: 100%;"></table>'
    );

    compareTable = $("#compare-modal table").DataTable({
        destroy: true,
        bSort: false,
        header: true,
        footer: true,
        paging: false,
        searching: false,
        info: false,
        data: dataSet.rows,
        columns: dataSet.columns,
        language: {
            "lengthMenu": "Mostrar _MENU_ resultados",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(Filtrados de _MAX_ registros totales)",
            "search": "Filtrar:",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        drawCallback: function (settings) {
            var template = "<div class='tooltip' style='opacity: 1 !important' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>";
            $('[data-toggle="tooltip"]').tooltip({template: template});
        },
    });

    $("#compare-modal").modal();
}

function ShowCompareDetail(dataSet) {
    compareTable = $("#results-container table").DataTable({
        destroy: true,
        bSort: false,
        header: true,
        footer: true,
        paging: false,
        searching: false,
        info: false,
        data: dataSet.rows,
        columns: dataSet.columns,
        language: {
            "lengthMenu": "Mostrar _MENU_ resultados",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ entradas",
            "infoEmpty": "No existen registros",
            "infoFiltered": "(Filtrados de _MAX_ registros totales)",
            "search": "Filtrar:",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        drawCallback: function (settings) {
            var template = "<div class='tooltip' style='opacity: 1 !important' role='tooltip'><div class='arrow'></div><div class='tooltip-inner'></div></div>";
            $('[data-toggle="tooltip"]').tooltip({template: template});
            $("table thead").remove();
        },
    });

    // Load the image in the container, look for a valid one
    var imageUrl = "/static/img/invalid-product.png";
    $(dataSet.images).each(function(index, value) {
        if (value && value.indexOf("base64") == -1) {
            imageUrl = value;
            return false;
        }
    });
    var imageHtml = "<img src='" + imageUrl + "' class='img-thumbnail' width='300' onerror='this.onerror=null;this.src=\"/static/img/invalid-product.png\"'>";
    $("#product-detail .image-container").html(imageHtml);

    // Show the container
    $("#results-container").removeClass("d-none");
}

function Searcher(term, custom) {
    if (typeof custom != "boolean") {
        custom = false;
    }
    if (term) {
        $("#result-page").loading({
            message: "Cargando los últimos precios <div class='mt-2'><i class='fa fa-spinner fa-spin fa-3x fa-fw'></i></div>",
            theme: "light",
            stoppable: false,
        });
        $.ajax({
            url: "/search",
            dataType: "json",
            data: {
                term: term,
                custom: custom
            },
            success: function (status) {
                if (status['code'] == 200) {
                    var validate = status['validate'];
                    var html = status['html'];
                    var companies = status['companies'];
                    var brands = status['brands'];
                    var minPrice = status['min_price'];
                    var maxPrice = status['max_price'];
                    var fixedTerm = status['term'];

                    // Clear companies filter
                    $("#span-comp input:checkbox").prop("checked", "checked");
                    $("#span-comp input:checkbox").trigger("click");

                    // Build slide and dropdowns
                    buildSlider(minPrice, maxPrice);
                    buildCompaniesDropdown(companies);
                    buildBrandsDropdown(brands);

                    // Display new data
                    ShowData(html, validate);

                    // Fill search input with current term
                    $("#search_main").val(fixedTerm);
                    $("#search_custom").val(custom);

                    getFavoriteSearches();
                }
            },
            error: function(response) {
                $("#result-page").loading("stop");
            }
        });
    }
}

function Comparer(productNames, coords, single) {
    if (typeof productNames == "string") {
        productNames = [productNames]
    }

    $.ajax({
        url: "/compare-products",
        dataType: "json",
        data: {
            coords: coords,
            products: productNames,
            single: single
        },
        success: function (status) {
            if (status['code'] == 200) {
                var validate = status['validate'];
                var dataSet = status['data'];
                if (single == true) {
                    if ($.isEmptyObject(dataSet)) {
                        $.redirect("/ ", {}, "get");
                    } else {
                        ShowCompareDetail(dataSet);
                    }
                } else {
                    ShowCompareModal(dataSet);
                }
            }
        }
    });
}

function drawRoute(origin, destination) {
    if (route) {
        map.removeControl(route);
    }

    var waypoints = [origin, destination];
    route = L.Routing.control({
        serviceUrl: "https://sitak.prix.tips/route/v1",
        waypoints: waypoints,
        lineOptions: {
            styles: [
                {
                    color: 'black',
                    opacity: 0.15,
                    weight: 9
                },
                {
                    color: 'white',
                    opacity: 0.8,
                    weight: 6
                },
                {
                    color: '#DC143C',
                    opacity: 1,
                    weight: 4
                }
            ]
        },
        createMarker: function (i, waypoints, n) {
            return;
        },
        routeWhileDragging: false,
        show: false,
        addWaypoints: false,
        draggableWaypoints: false
    });
    route.addTo(map);
}

function updateFavoriteSearch(term, custom, remove) {
    var coords = $("#coords").val();
    var url = "/add-search-to-favorites"
    if (remove) {
        url = "/remove-search-from-favorites"
    }

    $.ajax({
        url: url,
        method: "post",
        dataType: "json",
        data: {
            coords: coords,
            term: term,
            custom: custom
        },
        success: function (response) {
            removeNotifications();

            // Update searches list
            updateUserSearchList(response.searches);

            $.notify({
                    "message": response.message
                },
                {
                    "allow_dismiss": false,
                    "mouse_over": "pause",
                    "placement": {
                        "from": "bottom",
                        "align": "center"
                    },
                    "z_index": 1060,
                    "type": "success",
                }
            );
        },
        error: function (response) {
            removeNotifications();
            $.notify({
                    "message": response.responseJSON.message
                },
                {
                    "allow_dismiss": false,
                    "mouse_over": "pause",
                    "placement": {
                        "from": "bottom",
                        "align": "center"
                    },
                    "z_index": 1060,
                    "type": response.responseJSON.type,
                }
            );
        }
    });
}

function getFavoriteSearches() {
    $.ajax({
        url: "/favorite-searches",
        dataType: "json",
        method: "get",
        data: {},
        success: function (searches) {
            // Populate custom searches
            var currentTerm = $("#search_main").val();
            updateUserSearchList(searches);
        }
    });
}

function searchFromFavorite(element) {
    var term = $(element).attr("data-term");
    var custom = $(element).attr("data-custom");
    $.redirect("/compare", {"term": term, "custom": custom}, "get");
}

function buyProduct(event) {
    var productName = $(event.target).parents(".modal").find("div[data-jplist-item]").attr("data-product-name");
    var productCompany = $(event.target).attr("data-company");
    var coords = $("#coords").val();
    var session = $("#session").val();

    if (!session) {
        event.preventDefault();
        removeNotifications();
        $.notify({
                "message": "Para comprar productos debes <a class='login-redirect' href='/account/login'>iniciar sesión</a>. ¿No tienes una cuenta? ¡<a href='/account/register'>Regístrate</a>! Es gratis."
            },
            {
                "allow_dismiss": false,
                "mouse_over": "pause",
                "placement": {
                    "from": "bottom",
                    "align": "center"
                },
                "z_index": 1060,
                "type": "warning",
            }
        );
    }

    // Save this event
    $.ajax({
        url: "/save-event",
        dataType: "json",
        method: "post",
        data: {
            coords: coords,
            type: "product:buy",
            product_name: productName,
            product_company: productCompany
        },
    });
}

function updateUserSearchList(searches) {
    var currentTerm = $("#search_main").val();
    var htmlSearches = "<ul class='list-unstyled'>";

    $(searches).each(function (index, value) {
        htmlSearches += "" +
            "<li onclick='searchFromFavorite(this)' data-term='" + value.term + "' data-custom='" + value.custom + "' style='color: #6C707C; cursor: pointer; margin-bottom: 7px;'>" +
            "<i class='text-warning fa fa-star fa-fw'></i>" +
            value.term
        "</li>";
    });
    htmlSearches += "</ul>"
    if (searches.length > 0) {
        $("#my-searches").html(htmlSearches);
    } else {
        $("#my-searches").html("<p style='color: #6C707C;'>No has guardado ninguna búsqueda</p>");
    }
}


function getMapData(origin) {
    $.ajax({
        url: "/map-data",
        dataType: "json",
        method: "get",
        data: {
            origin: origin
        },
        success: function(response) {
            mapData = response.data;
        }
    });
}


// Product trends

function displayTrends() {
    $.ajax({
        url: "/api/product-trends",
        method: "post",
        success: function (response) {
            var html = "<div class='row'>";
            var elements = response.trends.slice(0, 6);
            var perRow = 3;
            $(elements).each(function(index, element) {
                html +=
                    "<div class='col text-center'>" +
                        "<img src='" + element.image +"' class='card-img-top' alt='" + element.name.toUpperCase() + "' style='width: 100px!important; cursor: pointer;' data-trend=" + element.name.toUpperCase() + " onclick='searchFromTrend(this);'>" +
                        "<div class='mt-2'>" +
                            "<p class='small font-weight-bold' style='color: #16CDCD;'>" + element.name.toUpperCase() +"</p>" +
                        "</div>" +
                    "</div>";
                if ((index + 1) % perRow == 0) {
                  html += "</div><div class='row'>";
                }
            });
            html += "</div>"
            $("#trends").html(html);
        },
    });
}

function searchFromTrend(element) {
    var trendTerm = $(element).attr("data-trend");

    // Save this event
    $.ajax({
        url: "/save-event",
        dataType: "json",
        method: "post",
        data: {
            type: "feature:trend",
            search_term: trendTerm
        },
    });

    $.redirect("/compare", {"term": trendTerm, "custom": true}, "get");
}
