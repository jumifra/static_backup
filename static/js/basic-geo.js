var $ = jQuery;

var map = L.map('map').fitWorld();
var maxZoom = 17;

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: maxZoom,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


function dragEnd(e) {
    var changedPos = e.target.getLatLng();
    this.bindPopup("Mi Domicilio").openPopup();
    $('#home-coords').val([changedPos.lat, changedPos.lng]);
    $('#work-coords').val([changedPos.lat, changedPos.lng]);
}

function onLocationFound(e) {
    var radius = e.accuracy / 2;
    currentMarker = L.marker(e.latlng, {draggable: true}).addTo(map).bindPopup("Mi Domicilio").openPopup();
    currentMarker.on("dragend", dragEnd);

    // Prefilled coords only if there's no current value, set the current value otherwise
    var currentHomeCoords = $("#home-coords").val();
    if (currentHomeCoords) {
      var lat = currentHomeCoords.split(",")[0];
      var lng = currentHomeCoords.split(",")[1];
      currentMarker.setLatLng([lat, lng, maxZoom]);
      map.panTo([lat, lng]);
    } else {
      $('#home-coords').val([e.latlng.lat, e.latlng.lng]);
    }

    var currentWorkCoords = $("#work-coords").val();
    if (currentWorkCoords) {
      var lat = currentWorkCoords.split(",")[0];
      var lng = currentWorkCoords.split(",")[1];
      currentMarker.setLatLng([lat, lng, maxZoom]);
      map.panTo([lat, lng]);
    } else {
      $('#work-coords').val([e.latlng.lat, e.latlng.lng]);
    }
}

function onLocationError(e) {
    alert(e.message);
}

map.on('locationfound', onLocationFound);
map.on('locationerror', onLocationError);

map.scrollWheelZoom.disable()
map.on('dblclick', function() {
  if (map.scrollWheelZoom.enabled()) {
      map.scrollWheelZoom.disable();
  }
  else {
    map.scrollWheelZoom.enable();
  }
});

map.locate({setView: true, watch: false, enableHighAccuracy: false, maxZoom: maxZoom});
