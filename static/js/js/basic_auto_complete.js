var fuzzyhoundProducts = new FuzzySearch({output_limit: 6, output_map: "alias"});

function itemTemplateTerms(suggestion) {
    var span = suggestion.label;
    return [
        "<div>",
            "<span class='suggestion'>", span, "</span>",
        "</div>"
    ].join("");

}

function itemTemplateProducts(suggestion) {
    return [
        "<div>",
            "<span class='title'>", fuzzyhoundProducts.highlight(suggestion.title, "title"), "</span>",
        "</div>"
    ].join("");
}

function footerTemplate() {
}

var $el = $("#search_main");
if ($el.length) {
    $el.autocomplete({
        minLength: 3,
        delay: 0,
        source: function (request, response) {
            $.ajax({
                method: "POST",
                dataType:"JSON",
                url: "https://mayflower.prix.tips",
                data: JSON.stringify({
                    "term": request.term,
                    "limit": 6
                }),
                success: function(resp) {
                    response(resp.words);
                    $(".suggestion").mark(request.term, {
                        "element": "span",
                        "className": "searchMark"
                    });
                }
            });
        },
        select: function (event, ui) {
            term = ui.item.value;
            $.redirect("/compare", {"term": term, "custom": false}, "get");
        }
    });

    var $inst = $el.autocomplete("instance");

    $inst._renderItem = function (ul, item) {
        return $("<li>").append(itemTemplateTerms(item)).appendTo(ul);
    };

    $inst._renderMenu = function (ul, items) {
        var nbitems = items.length, i = -1;
        while (++i < nbitems) {
            var display = items[i].label;
            this._renderItem(ul, items[i]).data("ui-autocomplete-item", {display: display, value: display});
        }
        $("<li>").append(footerTemplate()).data("ui-autocomplete-item", {display: "", value: ""}).appendTo(ul);
    };
}

var $el = $("#search_community");
if ($el.length) {
    $el.autocomplete({
        minLength: 3,
        delay: 0,
        source: function (request, response) {
            response(fuzzyhoundProducts.search(request.term));
        },
        select: function (event, ui) {
            var coords = $("#coords").val();
            var display = ui.item.display;
            var value = ui.item.value;
            $("#product").val(display);

            // Save this event
            $.ajax({
                url: "/save-event",
                dataType: "json",
                method: "post",
                data: {
                    coords: coords,
                    type: "community:product",
                    product_id: display,
                    product_name: value
                },
            });
        }
    });

    var $inst = $el.autocomplete("instance");

    $inst._renderItem = function (ul, item) {
        return $("<li>").append(itemTemplateProducts(item)).appendTo(ul);
    };

    $inst._renderMenu = function (ul, items) {
        var nbitems = items.length, i = -1;
        while (++i < nbitems) {
            var display = items[i].title;
            var value = items[i].id;
            this._renderItem(ul, items[i]).data("ui-autocomplete-item", {display: value, value: display});
        }
        $("<li>").append(footerTemplate()).data("ui-autocomplete-item", {display: "", value: ""}).appendTo(ul);
    };
}
